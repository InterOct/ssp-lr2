﻿namespace LR2
{
    internal class Food
    {
        public Food(long id, string name, int weight, double price)
        {
            Id = id;
            Name = name;
            Weight = weight;
            Price = price;
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public int Weight { get; set; }

        public double Price { get; set; }
    }
}