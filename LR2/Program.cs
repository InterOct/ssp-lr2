﻿using System;
using System.Xml;
using System.Xml.Linq;

namespace LR2
{
    internal class Program
    {
        private const string Base = @"E:\BSUIR\2016-2017\2\SSP\lr-2\LR2\";
        private const string XmlPath = Base + "Food.xml";

        private static readonly XmlDocument Document = new XmlDocument();

        private static void Main(string[] args)
        {
            Document.Load(XmlPath);
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Add new XML attribute");
                Console.WriteLine("2. Print document");
                Console.WriteLine("3. Exit");
                Console.Write("Your choise: ");
                var key = Console.ReadKey();
                Console.WriteLine();
                switch (key.KeyChar)
                {
                    case '1':
                        Console.Write("Index: ");
                        var index = int.Parse(Console.ReadLine());

                        Console.Write("Attribute name: ");
                        var name = Console.ReadLine();
                        Console.Write("Attribute value: ");
                        var value = Console.ReadLine();

                        CreateAttribute(index, name, value);
                        break;
                    case '2':
                        WriteToConsole();
                        break;
                    case '3':
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Please try again.");
                        break;
                }
                Console.ReadLine();
            }
        }

        private static void CreateAttribute(int index, string name, string value)
        {
            var food = Document.GetElementsByTagName("Food");
            if (food.Count >= index && index >= 0)
            {
                var attr = Document.CreateAttribute(name);
                attr.Value = value;
                food.Item(index).Attributes.Append(attr);
            }
            else
            {
                Console.WriteLine("Please try again.");
            }
        }

        private static void WriteToConsole()
        {
            var settings = new XmlWriterSettings { Indent = true };
            Document.Save(XmlWriter.Create(Console.Out, settings));
            Console.WriteLine();
        }
    }
}